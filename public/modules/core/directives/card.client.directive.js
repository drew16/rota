'use strict';

angular.module('core').directive('card', ['$document',
	function($document) {
		return {
			templateUrl: '/modules/core/views/card.client.view.html',
			restrict: 'C',
			scope: {
				model: '=ngModel',
				index: '='
			},
			link: function postLink(scope, element, attrs) {
				// Card directive logic
				// ...


				// scale the card depending on its position in the stack
				scope.$watch('index', function() {

					if (scope.index === 1) {
						element.css({
							width: element.parent().width(),
							left: 0,
							top: 0,
							visibility: 'visible'
						});
					}
					else if (scope.index > 1 && scope.index < 4) {
						element.css({
								width: element.parent().width() - 10 * (scope.index - 1),
								left: 5 * (scope.index - 1),
								top: 15 * (scope.index - 1),
								visibility: 'visible'
						});
					} else {
						element.css({
							visibility: 'hidden'
						});
					}

				});

				var mouseLastX;
				var swipeThreshold = element.parent().width() / 2;

				 // 1 = selected, -1 = rejected, 0 = undecided
				var status = 0;

				// adapt touch events so can be handled like mouse events
				var adaptMobileEvent = function(e) {
					e.screenX = e.screenX || (e.originalEvent.touches[0].screenX);
				};

				var moveHandler = function(e) {
					adaptMobileEvent(e);

					var left = parseInt(element.css('left'));
					var newLeft = left + (e.screenX - mouseLastX);
					element.css('left', newLeft);

					if (Math.abs(newLeft) > swipeThreshold) {

						if (newLeft < 0) {
							status = -1;
							element.addClass('tentative-no');
						} else {
							status = 1;
							element.addClass('tentative-yes');
						}
					} else {
						status = 0;
						element.removeClass('tentative-yes tentative-no');
					}

					mouseLastX = e.screenX;
				};

				var upHandler = function(e) {
					if (e) {
						e.preventDefault();
						e.stopPropagation();
					}

					if (status === 0) {
						element.css('left', 0);
					} else {
						scope.$apply(function() {
							scope.model.accepted = (status > 0);
						});
					}

					element.unbind('mousemove', moveHandler);
					element.unbind('mouseup', upHandler);

					element.unbind('touchmove', moveHandler);
					$document.unbind('touchcancel', upHandler);
					$document.unbind('touchend', upHandler);

				};

				var downHandler = function(e) {
					adaptMobileEvent(e);
					e.preventDefault();
					mouseLastX = e.screenX;

					element.bind('mousemove', moveHandler);
					element.bind('mouseup', upHandler);

					element.bind('touchmove', moveHandler);
					$document.bind('touchcancel', upHandler);
					$document.bind('touchend', upHandler);
				};


				element.bind('touchstart mousedown', downHandler);


			}
		};
	}
]);
