'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication', '$state', '$window',
	function($scope, Authentication, $state, $window) {
		// This provides Authentication context.
		$scope.authentication = Authentication;


		$scope.cards = [{
				name: 'Someone Else',
				rating: 4,
				accepted: null
			},
			{
				name: 'Random Guy',
				rating: 1,
				accepted: null
			}, {
				name: 'Wile E Coyote',
				rating: 2,
				accepted: null
			}, {
				name: 'Bugs Bunny',
				rating: 3,
				accepted: null
			}, {
				name: 'Drew Rathbone',
				rating: 4,
				accepted: null
			}, {
				name: 'Henry Kirkness',
				rating: 4,
				accepted: null
			}
		];

		$scope.undecided = function(card) {
			return card.accepted === null;
		};


	}
]);
